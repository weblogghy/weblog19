module.exports = {
  siteMetadata: {
    title: 'WeBlog Online - Learn from the Experts'
  },
  plugins: [
  {
    resolve: `gatsby-plugin-react-helmet`
  }
  ]
}
