import React from 'react'
import Link from 'gatsby-link'
import Helmet from 'react-helmet'
import Header from '../components/Header'
import SocialFooter from '../components/SocialMediaFooter'
import Footer from '../components/footer'
import refill from '../thank_you_referral.png'
import jgill from '../thank_you_join_group.png'


const ThankyouPage = () => (
  <div>
        <Helmet>
          <script>
          {`
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '724391948006821');
  fbq('track', 'PageView');
          `}
          </script>
          <script async src="https://www.googletagmanager.com/gtag/js?id=UA-143397376-2"></script>
          <script>
            {`
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            
            gtag('config', 'UA-143397376-2');
            `}
          </script>
        </Helmet>
        <script
          dangerouslySetInnerHTML={{
            __html: `
              fbq('track', 'Purchase');
            `
          }}
         >

        </script>
    <div className="w-90 wb-maxwidth-ns center cf">
    <Header/>
      <div className="cf w-100 fl pb3 pb5-ns pt3 pt5-ns bg-wb-grey-4">
        <div className="fl w-100 w-30-l">
          <h1 className="f3 f1-ns f-title-ns b ma0 lh-solid"> You're all set! </h1>
          <p className="f6 f6-ns lh-copy measure wb-grey-2 db pt1">
            Dear Participant,<br/>
            Your slot is confirmed and you are all set to be a part of WeBlog Online! 
            Please find your summit ticket in the email id you provided while making your reservation.
            <br/>
            <br/>
            We will meet again on the day of the event.
            <br/>
            <br/>
            For any queries kindly reach out to us through the contact numbers given below.
          </p>
        </div>
        <div className="mt4 fr w-100 w-60-l mt2-m mt0-ns">
          <iframe
            src="https://player.vimeo.com/video/355674409"
            width="100%" 
            className="youtube-video"
            frameBorder="0"
            allowFullScreen>
          </iframe>
        </div>
      </div>
      <div id="thankyou-page" className="w-100 fl pb3 pb5-ns pt2 bg-wb-grey-4">
        <h2 className="f3 f-title-ns b ma0 lh-solid"> There's more! </h2>

        <div className="w-100 cf pt4">
          <div className="fl w-100 w-50-ns">
            <h3 className="f4"> We pay you when a friend joins </h3>
            <p className="f6">
Join us as an affiliate and earn 30% commission each time your friend joins the event.
To know more, click on the affiliate button below and follow the instructions
            </p>
            <a href="/refer" className="b link tc bg-wb-blue-2 white ph3 pv3 mv3 ttu db w-100 w-40-l">
                Get Started
            </a>
          </div>
          <div className="fl w-100 w-50-ns tc">
            <img className="h5" src={refill}/>
          </div>
        </div>
        <div className="w-100 cf pt4">
          <div className="fl w-100 w-50-ns">
            <h3 className="f4"> Join the facebook group </h3>
            <p className="f6">
              Join our exclusive facebook group which is only for the participants. The group will be used for discussion, Live sessions with the host and a few speakers. Also, all updates related to the event will be first announced in the group
            </p>
            <a target="_blank" href="https://www.facebook.com/groups/2633187226726573/" className="b tc bg-wb-blue-2 white ph3 pv3 mv3 ttu db w-100 w-40-l link">
                Join Group
            </a>
          </div>
          <div className="fl w-100 w-50-ns tc">
            <img className="h5" src={jgill}/>
          </div>
        </div>

      </div>
    </div>
    <SocialFooter/>
    <Footer/>
  </div>
)

export default ThankyouPage
