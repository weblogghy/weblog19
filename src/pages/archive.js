import React from 'react'
import Link from 'gatsby-link'
import Header from '../components/Header'
import Footer from '../components/footer'
import Archive from '../components/archive';
import a17 from '../archive_2017.jpg'
import a18 from '../archive_2018.jpg'

const ArchivePage = () => (
  <div>
    <div className="w-90 wb-maxwidth-ns center cf">
    <Header/>
    <div className="w-90 wb-maxwidth-ns center">
      <Archive year="2018" img={a18}/>
      <Archive year="2017" img={a17}/>
    </div>
    <Footer/>
  </div>
  </div>
)

export default ArchivePage
