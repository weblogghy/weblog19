import React from 'react'
import Link from 'gatsby-link'
import Header from '../components/Header'
import Footer from '../components/footer'
import rishi from '../teamphotos/rishi.jpg'
import indra from '../teamphotos/indra.jpg'
import avi from '../teamphotos/avi.jpg'
import abhi from '../teamphotos/abhi.jpg'
import ash from '../teamphotos/ash.jpg'
import ang from '../teamphotos/ang.jpeg'
import tsh from '../teamphotos/tsh.jpeg'

const fb = "https://cdnjs.cloudflare.com/ajax/libs/ionicons/4.5.6/collection/build/ionicons/svg/logo-facebook.svg"
const tw = "https://cdnjs.cloudflare.com/ajax/libs/ionicons/4.5.6/collection/build/ionicons/svg/logo-twitter.svg"
const ig = "https://cdnjs.cloudflare.com/ajax/libs/ionicons/4.5.6/collection/build/ionicons/svg/logo-instagram.svg"

const Person = ({name, pos, img, f, t, i}) => (
  <div className="fl w-100 w-30-ns mb3 mr0 mr3-ns">
    <img src={img}/>
    <p className="f6 f5-ns">{name}</p>
    { f && <a href={f} className="link"> <img src={fb} className="h2"/> </a> }
    { t && <a href={t} className="link"> <img src={tw} className="h2"/> </a> }
    { i && <a href={i} className="link"> <img src={ig} className="h2"/> </a> }
  </div>
)
const SponsorPage = () => (
  <div>
    <div className="w-90 wb-maxwidth-ns center cf">
    <Header/>
    <div className="wb-maxwidth-ns center">
      <h3 className="f3 f1-ns mb0">Meet the team</h3>
      <p className="f6 f5-ns wb-grey-2 lh-copy w-100 w-80-ns">
        No one can whistle a symphony alone, it takes an orchestra to play it. And our orchestra comprises of 6 members working hard with one aim i.e. to bridge the gap between talent and opportunities through WeBlog.
      </p>
      <div className="cf">
        <Person name="Abhilash Sarma" img={abhi} i="asa" f="sd" t="sad"/>
        <Person name="Angshuman Dev Talukdar" img={ang} i="asa" f="sd" t="sad"/>
        <Person name="Ashish Sharma" img={ash} i="asa" f="sd" t="sad"/>
        <Person name="Avinash Barua" img={avi} i="asa" f="sd" t="sad"/>
        <Person name="Hrishikesh Barman" img={rishi} t="https://twitter.com/geekodour"/>
        <Person name="Indrajeet Bhuyan" img={indra} i="asa" f="sd" t="sad"/>
      </div>
    </div>
    <Footer/>
  </div>
  </div>
)

export default SponsorPage
