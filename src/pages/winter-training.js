import React from 'react'
import Link from 'gatsby-link'
import Header from '../components/Header_2'
import team from '../teamphotos/team.jpg'
import Helmet from 'react-helmet'
import Footer from '../components/footer'

const WinterTraining = () => (
  <div>
    <Helmet
      title="WeBlog Winter Training | 2019"
      meta={[
        { name: 'keywords', content: 'blogging, guwahati, assam, northeast, event, internet marketing' },
        { name: 'title', content: 'WeBlog Winter Training | 2019' },
        { property: 'og:type', content:"website" },
        { property: 'og:title', content:"WeBlog Winter Training | 2019" },
      ]}
      >
    </Helmet>
    <div className="w-90 wb-maxwidth-ns center cf">
    <Header/>
    <div className="wb-maxwidth-ns center">
      <h1 className="f3 f1-ns"> WeBlog Online Winter Training</h1>
      <h3 className="f4 f3-ns">About WeBlog</h3>
      <div className="w-100 cf">


<div className="fl w-100 w-50-ns">

        <p className="f6 f6-ns lh-copy measure wb-grey-2 db pt1 mb2">
      We started the WeBlog event back in the year 2017 intending to help people get started with their online journey. Over the last two years, we have been inviting many well-renowned speakers to our event who taught our participants and shared their career experiences. </p>
        <p className="f6 f6-ns lh-copy measure wb-grey-2 db pt1 mb2">
      We had some of the top bloggers, affiliate marketers, influencers as our speakers. 100s of participants who attended our event are now earning online. We are well known for the quality content that we give to our participants. </p>

</div>
<div className="fl w-100 w-50-ns pl0 pl3-ns">
  <img src={team}/>
</div>


      </div>

      <h3 className="f3">All the students will get:</h3>
      <div className="wb-grey-2">
        <ul>
<li>Lifetime access to WeBlog 2019 course</li>
<li>Access to a private facebook group where they can connect with others and discuss</li>
<li>Certificate</li>
        </ul>
  </div>


      <h3 className="f3">About the training:</h3>
      <div className="wb-grey-2">

      Duration : 14 days <br/> Date : 16-29 December, 2019<br/> Venue : Online <br/>
      Fee : ₹ 3800 <br/>
      Topics :
      <ul>
        <li>Blogging and SEO</li>
        <li>Instagram, YouTube, and Personal Branding</li>
        <li>Stock Market and Smart financing</li>
        <li>Affiliate Marketing and online earning</li>
        <li>Dropshipping and E-commerce</li>
        <li>Career roadmap</li>
      </ul>
      </div>

        <p className="f6 f6-ns lh-copy measure wb-grey-2 db pt1 mb2">
      After the completion of the topics, all the students will be assigned live projects on the above-mentioned topics. Also, there will be a live doubt clearing session everyday.

All the participants will get access to our WeBlog 2019 course for free which contains sessions and interviews of some of the top bloggers and digital marketers of the country.
</p>

<a className="mt5 link b db w-60-ns pointer w-100 pv3 tc ph5-ns black dim bg-wb-yellow ttu" href="https://www.instamojo.com/weblog/winter-training-at-weblog-2019/">
Register for Training
</a>

    </div>
    <Footer/>
  </div>
  </div>
)

export default WinterTraining 