import React from 'react'
import Link from 'gatsby-link'
import Helmet from 'react-helmet'
import Header from '../components/Header'
import SocialFooter from '../components/SocialMediaFooter'
import Footer from '../components/footer'
import refill from '../thank_you_referral.png'
import jgill from '../thank_you_join_group.png'


const ReferPage = () => (
  <div>
        <Helmet>
          <script>
          {`
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '2288410971271472');
  fbq('track', 'PageView');
          `}
          </script>
          <script async src="https://www.googletagmanager.com/gtag/js?id=UA-143397376-2"></script>
          <script>
            {`
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            
            gtag('config', 'UA-143397376-2');
            `}
          </script>
        </Helmet>
    <div className="w-90 wb-maxwidth-ns center cf">
    <Header/>
      <div className="cf w-100 fl pb3 pb5-ns pt3 pt5-ns bg-wb-grey-4">
        <div className="fl w-100">
          <h1 className="mb3 f3 f1-ns f-title-ns b ma0 lh-solid"> Earn 30% commission<sup>*</sup></h1>
          <h1 className="mb3 f5 f3-ns wb-grey-2 f-title-ns b ma0 lh-solid"> each time your friend joins through your link</h1>
          <iframe
            src="https://player.vimeo.com/video/355675134"
            width="100%" 
            className="youtube-video"
            frameBorder="0"
            allowFullScreen>
          </iframe>
          <p className="mt2 b"> Watch the video for a walkthrough of the referral process </p>
          <div className="f4 mv4">
            <span className="b">Step 1:</span>  Create instamojo account
            <a target="_blank" href="https://imjo.in/YJbctB" className="link tc db bg-wb-blue-2 white pv3 mv2 b pointer">Click here to create your instamojo account</a>
          </div>
          <div className="f4 mb3">
            <span className="b">Step 2:</span> Click the <span className="b">Join affiliate</span> button and login with Instamojo account.
          </div>
          <div className="f4 mb3">
            <span className="b">Step 3:</span> Inside the dashboard of Instamojo, check you affiliate tab
          </div>
          <div className="f4 mb3">
            <span className="b">Step 4:</span> You will get your unique affiliate Link, you can check all your sales inside the dashboard
          </div>
          <div className="f4 mb3">
            <span className="b">Step 5:</span> Copy that unique link and share with your friends and start earning
          </div>
          <div className="f4 mb3">
            <a target="_blank" href="https://www.instamojo.com/affiliate/subscribe/weblog-online/" className="tc db bg-wb-blue-2 white pv3 mv2 pointer link b">Join affiliate</a>
          </div>
        </div>
        <div className="mt4 fr w-100 w-60-l mt2-m mt0-ns">
        </div>
      </div>
      <div id="thankyou-page" className="w-100 fl pb3 pb5-ns pt2 bg-wb-grey-4">

        <div className="w-100 cf pt4">
          *Terms and conditions apply
        </div>

      </div>
    </div>
    <SocialFooter/>
    <Footer/>
  </div>
)

export default ReferPage
