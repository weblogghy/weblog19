import React from 'react'
import Link from 'gatsby-link'
import Helmet from 'react-helmet'
import Header from '../components/Header'
import Hero from '../components/Hero'
import TargetAud from '../components/TargetAud'
import Speakers from '../components/Speakers'
import Schedule from '../components/Schedule'
import Sponsors from '../components/sponsors'
import Supporter from '../components/supporters'
import Archive from '../components/archive'
import Footer from '../components/footer'
import Ticket from '../components/tickets'
import Banner from '../components/NEAbanner'
import TimeBanner from '../components/Timebanner'
import BookBanner from '../components/BookBanner'
import LastYear from '../components/LastYear'
import Testi from '../components/Testi'
import FooterBanner from '../components/FooterBannner'

class IndexPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      triggeredOnce: false,
      modalOpen: false
    };
    this.toggle = this.toggle.bind(this);
    this.close = this.close.bind(this);
    this.closeanddirect = this.closeanddirect.bind(this);
  }
  closeanddirect(){
      this.setState({
        modalOpen: false
      })
      window.location.replace("#buy-tickets")
  }
  close(){
      this.setState({
        modalOpen: false
      })
  }
  toggle(){

    if (!this.state.triggeredOnce) {
      this.setState({
        modalOpen: !this.state.modalOpen,
        triggeredOnce: true
      })
    }

  }
  componentDidMount(){
    document.addEventListener("mouseleave", this.toggle)
  }
  render() {
    const {modalOpen} = this.state
    return (
      <div className="popup cf">
        <Helmet>
          <script>
          {`
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '724391948006821');
  fbq('track', 'PageView');
          `}
          </script>
          <script async src="https://www.googletagmanager.com/gtag/js?id=UA-143397376-2"></script>
          <script>
            {`
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            
            gtag('config', 'UA-143397376-2');
            `}
          </script>
        </Helmet>
        <div className={`dn ${modalOpen?"db-l":"dn"} cf fixed left-0 top-0 w-100 h-100 ph1 z-1000 wholepage-pop`}>
          <div className="actual-pop center pv6 cf">
            <div className="bg-wb-blue-2 white center w-60-ns pv3">

              <div className="fr">
              <a onClick={this.close} className="w-100 tc w-30-ns pointer pa2 ttu bg-wb-yellow black">Close</a>
              </div>

              <div className="pa3">
              <h3 className="f3"> Here is what you get with your pass: </h3>
              <ul className="list">
                    <li className="mb2"> Practical sessions by industry experts - Learn from the best </li>
                    <li className="mb2"> Video recordings of all the sessions for a lifetime </li>
                    <li className="mb2"> Access to the members-only Facebook group </li>
                    <li className="mb2"> Technical support for setting up your blog/website </li>
                    <li className="mb2"> Exclusive interviews of top Bloggers, Digital marketers and Online entrepreneurs </li>
              </ul>
              </div>

              <a href="https://www.instamojo.com/weblog/weblog-online/"
              className="db link b mb3 self-end pointer tc w-100 pa2 ttu center bg-wb-yellow black">
              Click here to register <br/>
              (Early bird offer of 75% discount ends soon )
              </a>
            </div>
          </div>
        </div>
        {/* end of popup */}

        <div className="w-90 wb-maxwidth-ns center">
          <Header/>
          <Hero/>
          {/*<Banner/>*/}
          <TargetAud/>
        </div>

        {/* <TimeBanner/> */}

        <div className="w-90 wb-maxwidth-ns center">
          <Speakers/>
          <Schedule/>
        </div>
        <BookBanner/>
        <div className="w-90 wb-maxwidth-ns center">
          <LastYear />
          <Testi/>
        </div>
          <Ticket/>
          {/*<Supporter/>*/}
          <FooterBanner/>
          <Footer/>
      </div>
    )
  }
}

export default IndexPage
