import React from 'react'
import Link from 'gatsby-link'
import Header from '../components/Header'
import Footer from '../components/footer'
import Helmet from 'react-helmet'

const AwardPage = () => (
  <div className="w-90 w-100-ns center">
    <Helmet
      title="Northeast Creator Awards | WeBlog18"
      meta={[
        { name: 'description', content: 'Northeast Creator Awards | WeBlog Guwahati 2018' },
        { name: 'keywords', content: 'blogging,\
            guwahati, assam, northeast, event, awards' },
      ]}
    />
    <Header/>
    <article>
  <header className="">
    <div className="mw9 center pa2 pa4-ns pt5-ns ph4-l">
      <time className="f6 mb2 dib ttu tracked"><small>14 July, 2018</small></time>
      <h3 className="f2 measure-narrow lh-title mv0">
        <span className="wb-yellow lh-title white tracked-tight">
            Northeast Creator Awards 2018
        </span>
      </h3>
      <h5 className="f6 ttu tracked black-80">By WeBlog Team</h5>
    </div>
  </header>
  <div className="pa2 pv2-ns ph4-l mw9-l center">
      <div className="f5 f5-ns lh-copy measure">
      Are you a content creator? Do you know someone who deserves recognition for their awesome work? Tell them about #NECA! NorthEast Creator Awards will be honoring the best bloggers, Instagrammers and YouTubers. 

        <br/>
        <br/>
        Awards will be given for:
        <li> Best Lifestyle Blog </li>
        <li> Best Personal Blog </li>
        <li> Best Technology Blog </li>
        <li> Best Food Blog </li>
        <li> Best Travel Blog </li>
        <li> Best Art and Design Blog </li>
        <li> Other </li>
        
        <br/>
        <br/>
        You can do only one submission in one of the following ways:
        <li> Blog </li>
        <li> Instagram page </li>
        <li> Youtube Channel </li>
      </div>
      <p className="f5 f3-ns lh-copy measure"> The rules are simple, </p>
    <div className="f6 f5-ns lh-copy measure pl4 bl bw1 b--blue mb4">
        Submissions
        <ul>
          <li className="pb2">
            Submit Your Blog / Instagram Profile / Youtube Channel
          </li>
          <li className="pb2">
            Once submitted you will receive a confirmation mail from us.
          </li>
          <li className="pb2">
            After the submission deadline <span className="b">(August 22nd)</span>, 
            We will publish a page with all the nominations where your followers will be able to vote.
          </li>
        </ul>
        Judging
        <ul>
          <li className="pb2">
            Winner will be decided on the basis of Audience voting and Jury voting
          </li>
          <li className="pb2">
            21th August - 31st August, followers will be able to vote for their favorite creator.
          </li>
          <li className="pb2">
            40% vote from the audience will be counted and 60% vote from the Jury will be counted.
          </li>
          <li className="pb2">
            Winners will be announced on 1st September 2018. Awards and prizes will be given on the
            day of the event.
          </li>
        </ul>
    </div>
      <p className="f5 f3-ns lh-copy measure"> Prizes to be announced soon.</p>
  <a 
    className="f6 link dim ph4 pv3 mb2 dib black bg-wb-blue-dark white" 
    href="https://goo.gl/forms/ZGwgCUfiRYjsNdDc2">Register your blog</a>
  </div>
</article>
  <Footer/>
  </div>
)

export default AwardPage
