import React from 'react'
import Link from 'gatsby-link'

// Sperakers
import moonis from '../speakers/moonis_ali.jpg'
import naimish from '../speakers/naimish_sanghvi.jpg'
import harsh from '../speakers/harsh_agarwal.jpg'
import parag from '../speakers/parag_dutta.jpg'
import mohit from '../speakers/mohit_kumar.jpg'
import mukul from '../speakers/mukul_malik.jpg'
import deep from '../speakers/deepanker_verma.jpg'
import dev from '../speakers/dev.jpg'
import thor from '../speakers/thor.jpg'

class Person extends React.Component {
  constructor(props) {
    super(props);
    this.state = {modalOpen: false};
    this.toggle = this.toggle.bind(this);
  }
  toggle(){
    this.setState({
      modalOpen: !this.state.modalOpen
    })
  }
  render(){
    const {name, desg, photo, talk, desc} = this.props
    const {modalOpen} = this.state
    return (
      <div>
        <div className={`dn ${modalOpen?"db":"dn"} cf fixed left-0 top-0 w-100 h-100 ph1 z-1000 wholepage-pop`}>
          <div className="actual-pop center pv6 cf">
            <div className="bg-wb-blue-2 white center w-60-ns pv3">

              <div className="fr">
              <a onClick={this.toggle} className="w-100 b tc w-30-ns pointer pa2 ttu bg-wb-yellow black">Close</a>
              </div>

              <div className="pa3">
              <h3 className="f3-ns f4"> {name} </h3>
              <p className="b f5-ns f6"> {desg} </p>
              <p className="f6"> {desc} </p>
              </div>

            </div>
          </div>
        </div>

        <div className="pointer fl w-50 w-25-m w-25-l" onClick={this.toggle}>
         <div className="db link tc mr3">
          <img src={photo} className="w-100 db outline black-10"/>
          <dl className="mt2 f6 lh-copy">
            <dt className="tl clip">{name}</dt>
            <dd className="tl ml0 black truncate w-100">{name}</dd>
            <dt className="tl clip">{desc}</dt>
            <dd className="tl ml0 gray truncate w-100">{desg}</dd>
          </dl>
        </div>
        </div>
      </div>
    )
  }
}

const Speakers = () => (
    <div id="speakers">
      <div className="mw9 mt4 cf">
        <div>
          <div className="w-100 w-15-ns mb3">
            <div className="f5 f4-ns bg-white-ns b"> Speakers </div>
          </div>
          <div className="w-100 w-85-ns">
            <Person 
              name="Harsh Agarwal" 
              desg="Blogger" 
              photo={harsh} 
              desc="Harsh Agrawal is an Indian blogger,  who started his first blog as a hobby, and is now making millions of rupees by doing the same. He transparently discloses his blog income reports. He writes about SEO, social media, starting and managing a blog, and making money online. Harsh is the guy behind famous blog ShoutMeLoud.com"/>
            <Person 
              name="Moonis Ali" 
              desg="Ecom expert"
              photo={moonis} 
              desc="Moonis Ali is an e-commerce expert from india . He is the founder of Animus Digital Media & Marketing LLC, A digital marketing agency that helps brands & business with their online reputation, marketing and sales. Moonis operate multiple 7-Figure E-Commerce Brands on the Shopify platform, drop shipping goods from all over the world."/>
            <Person 
              name="Naimish Sanghvi" 
              desg="CoinCrunchIndia" 
              photo={naimish} 
              desc=""/>
            <Person 
              name="Devlina Bhatacharya" 
              desg="Influencer" 
              photo={dev} 
              desc="Devlina Bhattacharya, an Engineer who refuses to be one and chooses to be a funky monkey instead, a poetess, occasionally and a fashion and lifestyle influencer from Guwahati, North-East India."/>
            <Person 
              name="Thor Aarsand"
              desg="Online Entrepreneur"
              photo={thor}
              desc=""/>
            <Person 
              name="Parag Dutta" 
              desg="UnboxGuwahati" 
              photo={parag} 
              desc="Mr. Parag Dutta is the Co-founder of Unbox Guwahati - A guwahati based cloud kitchen. Unbox guwahati was launched with the idea to serve quality comfort food all night to make sure no one sleeps hungry!! It was launched at a time when swiggy or zomato and other aggregators was a far cry for the city. They started selling via Instagram. Even today they selling using Instagram."/>
            <Person 
              name="Mohit Kumar" 
              desg="The Hacker News" 
              photo={mohit} 
              desc="Founder of World's no.1 Security News website: TheHackerNews"/>
            <Person 
              name="Deepanker Verma" 
              desg="Techlomedia" 
              photo={deep} 
              desc=""/>
            <Person 
              name="Mukul Malik"
              desg="Finance Youtuber"
              photo={mukul}
              desc="Runs a youtube channel on finance which has Lakhs of followers"/>
          </div>
        </div>
      </div>
      <div className="tr">
          <span className="b f4"> More speakers coming soon. </span>
      </div>
    </div>
)

export default Speakers
