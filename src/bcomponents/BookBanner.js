import React from 'react'
import Link from 'gatsby-link'

export default () => (
    <div className="db wb-maxwidth mv5 cf bg-wb-blue-2 pv4 cf">
          <div className="fl white w-100 w-60-l tc tr-ns">
            <p className="f4">
            Learn how to earn online from the best people in the business.
            </p>
          </div>

          <div className="fl w-100 w-40-l tc tr-ns pr0 pr3-ns pv4">
            <a href="https://bit.ly/2PZIsXY"
            className="link b db di-ns w-100 pv3 tc ph5-ns black dim bg-wb-yellow ttu">
              Book your Seat
            </a>
          </div>

    </div>
)