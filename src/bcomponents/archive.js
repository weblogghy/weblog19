import React from 'react'
import Link from 'gatsby-link'

const Archive = ({year,img}) => (
      <div id="archive" className="mw9 mt4">
        <div className="cf">
          <div className="fl w-100 w-10-ns">
            <div className="bg-white-ns ttu b tracked-mega">
             Archive {year}
            </div>
          </div>
          <div className="fl w-100 w-90-ns tc">
            <img src={img}/>
          </div>
        </div>
      </div>
)

export default Archive