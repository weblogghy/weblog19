import React from 'react'
import Link from 'gatsby-link'

export default () => (
    <section className="dn db-ns pb5">
      <article className="mw8 center bg-black-30">
        <div className="dt-ns dt--fixed-ns w-100">
          <div className="pa3 pa4-ns dtc-ns v-mid">
            <div>
              <h2 className="fw4 black mt0 mb3">Northeast Creator Awards</h2>
              <p className="black-70 measure lh-copy mv0">
                Accepting entries till August 22nd 2018
              </p>
            </div>
          </div>
          <div className="pa3 pa4-ns dtc-ns v-mid">
            <Link 
              to="/neca"
              className="no-underline f6 tc db w-100 pv3 
              bg-animate bg-black dim white br2"
            > Register your blog
            </Link>
          </div>
        </div>
      </article>
    </section>
)
