import React from 'react'
import Link from 'gatsby-link'
//import CutCircle from '../CutCircle.png'

const Tickets = () => (
    <div className="w-100 bg-wb-blue-2">
      <div className="w-90 pv4 wb-maxwidth-ns center">
        <div className="db dt-ns mw9 w-100 cf">
          <div className="w-50-ns pb4 w-100 fl b white">
            Limited time early bird tickets <br/> Available at Nu. 3500
          </div>
          <div className="w-50-ns w-100 fl tr-ns mt3 tc tl-ns">
            <a href="https://bit.ly/2PZIsXY" className="link b db di-ns pv3 ph5 black dim bg-wb-yellow ttu">
              Book now
            </a>
          </div>
        </div>
      </div>
    </div>
)

export default Tickets