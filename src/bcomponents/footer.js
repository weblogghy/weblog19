import React from 'react'
import Link from 'gatsby-link'

const Footer = () => (
    <footer className="pv4 ph3 ph5-m ph6-l mid-gray">
  {/*<small className="f6 db tc">© 2018 <b className="ttu">WeBlogCon</b>., All Rights Reserved</small>*/}
  <div className="tc mt3">
    <a href="https://www.facebook.com/weblogcon/" className="f6 dib ph2 link mid-gray dim">Facebook</a>
    <a href="https://www.instagram.com/weblogcon/" className="f6 dib ph2 link mid-gray dim">Instagram</a>
    <a href="#"  className="f6 dib ph2 link mid-gray dim">+97517622732</a>
  </div>
</footer>
)

export default Footer