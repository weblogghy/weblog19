import React from 'react'
import Link from 'gatsby-link'
import ill from '../home_learn.png'
import aff from '../icons/affliate.png'
import blg from '../icons/blogging.png'
import ecom from '../icons/ecom.png'
import finan from '../icons/finance.png'
import grow from '../icons/growing.png'

const Session = ({icon,topic,desc}) => (
  <div className="fl w-90 mb3">
    <table>
      <tbody>
    <tr>
      <td>
        <img className="h2 h3-ns" src={icon}/>
      </td>
      <td className="f4 pl2">{topic}</td>
    </tr>
    <tr>
      <td className="h2 h3-ns w2 w3-ns"></td>
      <td className="f6 pl2 wb-grey-2 lh-copy">
      {desc}
      </td>
    </tr>
      </tbody>
    </table>
  </div>
)

const Schedule = () => (
    <div id="schedule">
        <div className="pv4">
          <div className="f5 f4-ns bg-white-ns b"> What you'll learn </div>
        </div>
        <div className="cf">

          <div className="fl w-100 w-50-ns cf">
            <Session icon={blg} topic="Blogging and SEO" desc="Getting started, content writing, from blog to brand, interviews with renowned bloggers"/>
            <Session icon={finan} topic="Stock Market and Smart financing" desc="Deep dive into smart financing, interview with CEO of an exchange, investment advice for millenials, mutual assets and stock market"/>
            <Session icon={ecom} topic="Dropshipping and E-commerce" desc="Getting started, dropshipping in India, interviews with successful dropshippers and e-commerce businessmen"/>
          </div>
          <div className="fl w-100 w-50-ns cf">
            <Session icon={grow} topic="Instagram, YouTube and Personal Branding" desc="Growing on Instagram, YouTube, interviews with YouTubers and influencers, personal branding"/>
            <Session icon={aff} topic="Affiliate Marketing and online earning" desc="How to make money with affiliate marketing, freelancing, cloud kitchen, advice from successful affiiate marketers"/>
            <div className="fl w-100 tc">
              <img src={ill} className="h-auto h-auto-m ill-height-ns"/>
            </div>
          </div>

      </div>
    </div>
)

export default Schedule