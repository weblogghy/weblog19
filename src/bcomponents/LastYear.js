import React from 'react'
import Link from 'gatsby-link'

const Hero = () => (
    <div className="cf w-100 fl pb3 pb5-ns pt3 pt5-ns bg-wb-grey-4">
      <div className="fl w-100 w-30-l">
        <h1 className="f4 f-title-ns b ma0 lh-solid"> WeBlog 2018 </h1>
        <p className="f4 f-title-ns mt4">
          A Look at what happened last year
        </p>
        <p className="f6 f6-ns lh-copy measure wb-grey-2 db pt1">
          WeBlog began 2 years ago as an annual event in Guwahati, but with the overwhelming response and support we received, we finally decided to take it online this year to reach out to more people. Here’s a small peak at what happened last year.
        </p>
      </div>
      <div className="mt4 fr w-100 w-60-l mt2-m mt0-ns">
        <iframe
          width="100%"
          className="youtube-video"
          src="https://player.vimeo.com/video/355674325"
          frameBorder="0"
          allowFullScreen>
        </iframe>
      </div>
    </div>
)

export default Hero