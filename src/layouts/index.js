import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import Favicon from '../favicon.ico'

import './tachyon/css/tachyons.css'
import './plyr.css'
import './custom.css'

const Layout = ({ children, data }) => (
  <div>
    <Helmet
      title={data.site.siteMetadata.title}
      meta={[
        { name: 'keywords', content: 'blogging, guwahati, assam, northeast, event, internet marketing' },
        { name: 'title', content: 'WeBlog Online 2019 - Learn from the Experts' },
        { name: 'description', content: 'Join WeBlog 2019 to learn about blogging and digital marketing' },
        { property: 'og:type', content:"website" },
        { property: 'og:title', content:"WeBlog Online 2019 - Learn from the Experts" },
        { property: 'og:description', content:"Join WeBlog 2019 to learn about blogging and digital marketing" },
        { property: 'twitter:card', content:"summary_large_image" },
        { property: 'twitter:title', content:"WeBlog Online 2019 - Learn from the Experts" },
        { property: 'twitter:description', content:"Join WeBlog 2019 to learn about blogging and digital marketing" }
      ]}
    >
      <link rel="icon" type="image/png" href={Favicon} sizes="16x16" />
    </Helmet>
    <div
      style={{
        margin: '0 auto', paddingTop: 0, fontFamily: 'Roboto Mono,monospace'
      }}
    >
      {children()}
    </div>
  </div>
)

Layout.propTypes = {
  children: PropTypes.func,
}

export default Layout

export const query = graphql`
  query SiteTitleQuery {
    site {
      siteMetadata {
        title
      }
    }
  }
`
