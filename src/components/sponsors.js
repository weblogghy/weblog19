import React from 'react'
import Link from 'gatsby-link'

// sponsors
import SnoozePost from '../sponsors/snoozepost.jpg'
import Mnet from '../sponsors/mnet.png'
import nec from '../sponsors/nec.png'
import radiom from '../sponsors/radiom.png'
import gmc from '../sponsors/gmc.png'
import tgt from '../sponsors/tgt.png'
import del from '../sponsors/del.png'
import swag from '../sponsors/swag.png'

const SponsorItem = ({url}) => (
 <div className="fl w-25 w-15-ns pa2">
  <div>
  <img src={url} />
</div>
</div>
)

const Sponsors = () => (
      <div id="sponsors" className="mw9 mt5">
        <div className="cf">
          <div className="fl w-100 w-15-ns">
            <div className="bg-white-ns ttu b tracked-mega">
             Sponsors
            </div>
          </div>
          <div className="fl w-100 w-85-ns cf">
              <div className="db cf">
                <SponsorItem url={Mnet}/>
                <SponsorItem url={SnoozePost}/>
                <SponsorItem url={nec}/>
                <SponsorItem url={gmc}/>
                <SponsorItem url={tgt}/>
                <SponsorItem url={del}/>
                <SponsorItem url={swag}/>
              </div>
              <div className="">
                <p className="">We need more sponsors to make this happen. 
                <Link 
                  to="/sponsors"
                  className="bg-yellow black">Become a Sponsor.</Link></p>
              </div>
          </div>
        </div>
      </div>
)

export default Sponsors