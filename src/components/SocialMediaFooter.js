import React from 'react'
import Link from 'gatsby-link'
//import CutCircle from '../CutCircle.png'
const fb = "https://cdnjs.cloudflare.com/ajax/libs/ionicons/4.5.6/collection/build/ionicons/svg/logo-facebook.svg"
const ig = "https://cdnjs.cloudflare.com/ajax/libs/ionicons/4.5.6/collection/build/ionicons/svg/logo-instagram.svg"

const Tickets = () => (
    <div className="w-100 bg-wb-yellow">
      <div className="w-90 wb-maxwidth-ns center">
        <div className="db dt-ns mw9 center w-100 tl">
          <div className="dib dtc-ns v-mid link pv2">
            <p className="black b"> For updates on WeBlog'19. Connect with us on our social media </p>

            <a href="https://www.facebook.com/weblogcon/" className="f6 link dim ph3 pv2 mb2 dib black bg-white center v-mid">
              <table>
                <tbody>
                  <tr>
                    <td><img src={fb} className="h1"/></td>
                    <td>Facebook</td>
                  </tr>
                </tbody>
              </table>
            </a>
            <a href="https://www.instagram.com/weblogcon/"
            className="ml3 f6 link dim ph3 pv2 mb2 dib black bg-white v-mid">
              <table>
                <tbody>
                  <tr>
                    <td><img src={ig} className="h1"/></td>
                    <td>Instagram</td>
                  </tr>
                </tbody>
              </table>
            </a>
          </div>
        </div>
      </div>
    </div>
)

export default Tickets