import React from 'react'
import Link from 'gatsby-link'
import ticketill from '../ticket_illustration.png'

const Tickets = () => (
    <div id="buy-tickets" className="w-100 pv3 bg-wb-yellow">
      <div className="w-90 wb-maxwidth-ns center">
        <div className="db dt-ns mw9 center w-100">
          <div className="dib dtc-ns v-mid tl w-100 w-50-ns">
            <div className="pv4">
              <div className="f5 f4-ns b"> Join Now </div>
            </div>
            <div className="pb4">
              <div className="f5 f4-ns"> Let's get you started </div>
              <div className="f6 wb-grey-2 pt2"> Here's what you get from this online event</div>
              <ul className="pa0 pl3">
                <li className="mb2 f6 f5-ns">No travelling required, learn without leaving your comfort zone.</li>
                <li className="mb2 f6 f5-ns">Lifetime access to every single thing that will be taught be it interviews, tutorials and more.</li>
                <li className="mb2 f6 f5-ns">Chance to gain exclusive access to various online communities and grow connections across borders.</li>
                <li className="mb2 f6 f5-ns">Indulge yourselves in other work without any worries, the content is never running away.</li>
              </ul>
            </div>
          </div>
          <div className="db dtc-ns v-mid w-100 w-50-ns tl tr-ns mt2 mt0-ns pt3 pt0-ns tr">
            <img src={ticketill} className="h-auto h5-l"/>
          </div>
        </div>
      </div>
    </div>
)

export default Tickets