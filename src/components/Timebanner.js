import React from 'react'
import Link from 'gatsby-link'
import Compass from '../compass-regular.svg'

class TimeBanner extends React.Component {
  constructor(props) {
    super(props);
    this.state = {days: 0, hours: 0, minutes: 0};
    this.startcounter = this.startcounter.bind(this);
  }
  startcounter(){
    let countDownDate = new Date("September 29, 2019 15:37:25").getTime();
    let now = new Date().getTime();
    let distance = countDownDate - now;
    let days = Math.floor(distance / (1000 * 60 * 60 * 24));
    let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    if (hours < 10) {
      hours = "0"+hours
    }
    if (days < 10) {
      days = "0"+days
    }
    if (minutes < 10) {
      minutes = "0"+minutes
    }
    this.setState({ days: days, hours, minutes })
  }
  componentDidMount(){
    this.startcounter()
  }
  render(){
    return (
      <div className="db w-100">
        <div className="fr w-100 wb-timebanner-width-ns mb5 mt4">
            <div className="w-100 w-100-m w-50-ns fl">
              <div className="pl2">
                <h2 className="f2 f1-ns wb-grey-2 mt0 mb1 mb3-ns i b">Starts in</h2>
                <div className="tl">
                  <table>
                    <tbody>
                  <tr>
                    <td className="f2 fw3 wb-grey-2 lh-copy f1-ns i">{this.state.days}</td>
                    <td className="f3 fw3 wb-grey-2 ph2 ph4-ns">|</td>
                    <td className="f2 fw3 wb-grey-2 f1-ns i">{this.state.hours}</td>
                    <td className="f3 fw3 wb-grey-2 ph2 ph4-ns">|</td>
                    <td className="f2 fw3 wb-grey-2 f1-ns i">{this.state.minutes}</td>
                  </tr>
                  <tr>
                    <td className="f6 ttu b wb-grey-2">Days</td>
                    <td className="f6 ttu b wb-grey-2"></td>
                    <td className="f6 ttu b wb-grey-2">Hours</td>
                    <td className="f6 ttu b wb-grey-2"></td>
                    <td className="f6 ttu b wb-grey-2">Minutes</td>
                  </tr>

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <div className="w-100 w-100-m w-50-ns fl">
              <div className="f6 pl0 pl3-ns tc db w-100 pv3 bg-black dim bg-wb-yellow cf">
                <div className="w-20 fl">
                  <img className="h2 h3-ns pt3" src={Compass}/>
                </div>
                <div className="w-80 fl pl1 pl4-ns">
                  <p className="f5 b f4-ns mb0 tl"> Online, 29th Sept - 2nd Oct</p>
                  <p className="tl f6 wb-grey-2"> No travel no issues </p>
                </div>
              </div>
            </div>
        </div>
      </div>
    )
  }
}

export default TimeBanner