import React from 'react'
import Link from 'gatsby-link'

const Hero = () => (
    <div className="cf w-100 fl pb3 pb5-ns pt3 pt5-ns bg-wb-grey-4">
      <div className="fl w-100 w-40-l">
        <h1 className="f2 f1-ns f-title-ns b ma0 lh-solid">
            WeBlog <br/> Online <br/>
        </h1>
        <p className="f6 f6-ns lh-copy measure wb-grey-2 db pt1 mb2">
WeBlog is online this time! We aim to bring the best of the best Digital Marketers, Bloggers, Youtubers and finance experts to your screens with endless possibilities for you to excel. 
<br/>
<br/>
So, what are you waiting for?
        </p>

        <a href="https://www.instamojo.com/weblog/weblog-online/"
            style={{color: '#000000'}}
            className="db tl-ns tc f6 f5-ns ttu link dim w-90-ns ph1 ph3-ns pv3 b white bg-wb-yellow">
          Register now and get 75% OFF
        </a>
      </div>
      <div className="pl0 pl1-ns mt4 fr w-100 w-60-l mt2-m mt0-ns">
        <iframe
          src="https://player.vimeo.com/video/355674832"
          width="100%" 
          className="youtube-video"
          frameBorder="0"
          allowFullScreen>
        </iframe>
      </div>
    </div>
)

export default Hero

/*
<div class="mw9 center ph3-ns">
  <div class="cf ph2-ns">
    <div class="fl w-100 w-25-ns pa2">
      <div class="outline bg-white pv4"></div>
    </div>
    <div class="fl w-100 w-25-ns pa2">
      <div class="outline bg-white pv4"></div>
    </div>
    <div class="fl w-100 w-25-ns pa2">
      <div class="outline bg-white pv4"></div>
    </div>
    <div class="fl w-100 w-25-ns pa2">
      <div class="outline bg-white pv4"></div>
    </div>
  </div>
</div>
*/