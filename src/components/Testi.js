import React from 'react'
import Link from 'gatsby-link'
import ill from '../home_testimonials.png'

const Session = ({name,testi}) => (
  <div className="bl bw2 grey-border mb4 fl w-90">
    <table>
      <tbody>
    <tr>
      <td></td>
      <td className="f6 wb-grey-2 lh-copy">{testi}</td>
    </tr>
    <tr>
      <td></td>
      <td className="f6 b">- {name}</td>
    </tr>
      </tbody>
    </table>
  </div>
)

const Schedule = () => (
    <div id="schedule">
        <div className="pv4">
          <div className="f5 f4-ns bg-white-ns b"> Testimonials </div>
        </div>
        <div className="pb4">
          <div className="f5 f4-ns bg-white-ns"> Thank you for the love! </div>
        </div>
        <div className="cf">

          <div className="fl w-100 w-50-ns cf">
            <Session name="Daisinlung Sebastian Kamsuan" testi="WeBlog2018 was helpful for the beginners and insightful for those into active blogging. The programme was enriched by many resource persons. Hats off to the We-Blog Team for hosting such an innovative WeBlog programme."/>
            <Session name="Deepsikha Choudhury" testi="WeBlog sessions are always very interesting. Thanks for teaching us in detail about PoD and Facebook Ads. I would also like to learn about Google Ads and YouTube contents"/>
            <Session name="Rakendra Sah" testi="First of all thank you so much for creating such an environment in Guwahati. I have taken number of classes here but they were neither conceptual nor easy to understand. But you all make the topic very easy to understand, interesting, informative with every small details."/>
          </div>
          <div className="fl w-100 w-50-ns cf">
            <Session name="Archit Todi" testi="Its was a superb informative event. Will look forward for more events in coming future"/>
            <Session name="Rudra Deka" testi="Best option if one is choosing blogging as their career. Helps to form the idea of what blogging should be. Empowering young minds to voice their thoughts"/>
            <div className="fl w-100 tc">
              <img src={ill} className="h-auto h-auto-m ill-height-ns"/>
            </div>
          </div>

      </div>
    </div>
)

export default Schedule