import React from 'react'
import ill from '../home_for_who.png'


const TargetAud = () => (
      <div className="cf">
          <div className="w-100">
            <div className="f5 f4-ns bg-white-ns b"> For Who? </div>

            <div className="w-100 w-60-ns fl">
            <div className="f5 lh-copy f4-ns bg-white-ns pt1 pt4-ns">
              Personal and Professional Bloggers, Affiliate Marketers,
              Social Media Influencers,Content Managers, Social Media Managers,
              Entrepreneurs and Finance Enthusiasts.
            </div>
            <p className="f6 f6-ns lh-copy measure wb-grey-2 db">
              There's something for everyone.
            </p>
            </div>

            <div className="relative-ns tc w-90-s w-40-ns fn fl-ns">
              <img src={ill} className="h4 h-auto-m h5-l"/>
            </div>

          </div>
    </div>
)

export default TargetAud
