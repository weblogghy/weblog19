import React from 'react'
import Link from 'gatsby-link'
import Logo from '../logo.png'

const Header = () => (
<header className="w-100 pv3 ttu">
  <div className="center">
    <div className="db dt-ns mw9 center w-100">
      <div className="dib dtc-ns v-mid tl w-20">
        <a href="/" className="db" title="WeBlog"> 
          <img 
          className="h-auto h3-l b3"
          src={Logo}/>
        </a>
      </div>
      <nav className="db dtc-ns v-mid w-100 tl tr-ns mt2 mt0-ns pt3 pt0-ns">
        <a title="Our Speakers" href="/#speakers"
            className="f6 fw6 hover-blue link black mr2 mr3-m mr4-l dib">
          Speakers
        </a>
        <a title="Archives" href="/archive"
            className="f6 fw6 hover-blue link black mr2 mr3-m mr4-l dib">
          Archive
        </a>
        <a title="Team" href="/team"
            className="f6 fw6 hover-blue link black mr2 mr3-m mr4-l dib">
          Team
        </a>
        <a title="Team" href="/winter-training"
            className="f6 fw6 hover-blue link black mr2 mr3-m mr4-l dib">
          Training
        </a>
      </nav>
    </div>
  </div>
</header>
)

export default Header
