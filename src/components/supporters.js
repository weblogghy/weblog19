import React from 'react'
import Link from 'gatsby-link'

// sponsors
import ne8 from '../sponsors/ne8.jpg'

const SupporterItem = ({url}) => (
 <div className="fl w-25 w-10-ns pa2">
  <div>
  <img src={url} />
</div>
</div>
)

const Supporter = () => (
      <div id="supporter" className="mw9 mt5">
        <div className="cf">
          <div className="fl w-100 w-15-ns">
            <div className="bg-white-ns ttu b tracked-mega">
             Supporters
            </div>
          </div>
          <div className="fl w-100 w-85-ns cf">
              <div className="db cf">
                <SupporterItem url={ne8}/>
                <SupporterItem url={ne8}/>
                <SupporterItem url={ne8}/>
                <SupporterItem url={ne8}/>
                <SupporterItem url={ne8}/>
                <SupporterItem url={ne8}/>
                <SupporterItem url={ne8}/>
                <SupporterItem url={ne8}/>
                <SupporterItem url={ne8}/>
                <SupporterItem url={ne8}/>
                <SupporterItem url={ne8}/>
                <SupporterItem url={ne8}/>
              </div>
          </div>
        </div>
      </div>
)

export default Supporter